import React, { Component } from "react";
import Field from "../components/Field";
import Attempt from "../components/Attempt";
import Button from "../components/Button";
import '../styles/bootstrap.css';
import '../styles/style.css';

class App extends Component {

    state = {
        cell: this.getArrayCell(),
        gameState : 'running'
    }
    count = 0;

    getArrayCell() {
        let arrayCell = [];
        for (var i = 0; i < 36; i++) {
            arrayCell[i] = { index: i, hasItem: false, showCell: false}
        }
        var randomNumber = Math.floor(Math.random() * 36);
        arrayCell[randomNumber].hasItem = true;
        return arrayCell;
    }
    openCell = (id) => {
        var cells = { ...this.state.cell }
        if(cells[id].showCell === false && this.state.gameState !=='stopping')
        {
            this.count++;
        }
        if(cells[id].hasItem === true)
        {
          let gameStatus = 'stopping'
          this.setState({gameState:gameStatus})
        }
        if(this.state.gameState !== 'stopping')
        {
            cells[id].showCell = true;
            this.setState({ cells });
        }
    }
    resetField = () => {
        this.count = 0;
        this.setState({gameState: 'running'})
        this.setState({ cell: this.getArrayCell()});
    }

    render() {
        return (
            <div className="Game">
                <Field cell={this.state.cell} open={this.openCell} 
                reset={this.resetField}></Field>
                <Attempt count={this.count}></Attempt>
                <Button click={this.resetField}></Button>
            </div>
        );
    }
}
export default App; 