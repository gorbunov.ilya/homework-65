import React from "react";
const Cell = props => {
    
    let classes = ['cell']
    var item;
    if (props.showCell) {
        classes.push('open')
        if (props.hasItem) {
            item = '0';
            var cell = props.cellNumber;
            window.confirm(`Вы нашли 0 в ячейке ${cell+1} \nПримите поздравления!\nCбросьте игру для продолжения!`);
        }
    }
    return (
        <div className={classes.join(' ')} onClick={props.click}><p>{item}</p></div>
    );
}
export default Cell;